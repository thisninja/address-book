var express = require('express'),
    bodyParser = require('body-parser'),
    app = express();

var id = 7,
    data = {
        1: {id: 1, firstName: 'John', lastName: 'Doe', email: 'johndoe@test.test'},
        2: {id: 2, firstName: 'Eric', lastName: 'Donovan', email: 'ericdo@test.test'},
        3: {id: 3, firstName: 'Steve', lastName: 'Trim', email: 'stevetrim@test.test'},
        4: {id: 4, firstName: 'Sem', lastName: 'Petee', email: 'sempe@test.test'},
        5: {id: 5, firstName: 'Eliza', lastName: 'Olson', email: 'elio@test.test'},
        6: {id: 6, firstName: 'Rick', lastName: 'Green', email: 'rickgre@test.test'},
        7: {id: 7, firstName: 'Ann', lastName: 'Brown', email: 'annbro@test.test'}
    };

    app.use(bodyParser.json());
    app.use(express.static('./public'));

    app.route('/api/contacts')
        .get(function(req, res) {
            res.json(Object.keys(data).map(function(key) {
                return data[key];
            }));
        })
        .post(function(req, res) {
            var record = req.body;

            record.id = ++id;
            data[record.id] = record;
            res.json(record);
        });

    app.route('/api/contacts/:id')
        .get(function(req, res) {
            res.json(data[req.params.id]);
        })
        .put(function(req, res) {
            data[req.params.id] = req.body;
            res.json(req.body);
        })
        .delete(function(req, res) {
            delete data[req.params.id];
            res.json(null);
        });

    app.get('*', function(req, res) {
        res.sendFile(__dirname + '/public/index.html');
    });

    app.listen(9000);
