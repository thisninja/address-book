var app = angular.module('AddressBookApp', ['ngRoute', 'ngResource']);

// Configuring Router

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/contacts', {
            controller: 'ContactsListController',
            templateUrl: 'views/contacts-list.html'
        })
        .when('/contacts/new', {
            controller: 'NewContactController',
            templateUrl: 'views/new-contact.html'
        })
        .when('/contacts/:id', {
            controller: 'SingleContactController',
            templateUrl: 'views/single-contact.html'
        })
        .otherwise({
            redirectTo: '/contacts'
        });

        $locationProvider.html5Mode(true);
});

app.factory('Contacts', function($resource) {
    // @ stands for text
    return $resource('/api/contacts/:id', { id: '@id'}, {
        'update': { method: 'PUT'}
    });
});

app.controller('ContactsListController', function($scope, Contacts) {
    $scope.contacts = Contacts.query();
});

app.controller('SingleContactController', function($scope, Contacts, $routeParams, $location) {
    var contactId = parseInt($routeParams.id, 10);

    $scope.contact = Contacts.get({ id: contactId });

    $scope.updateContact = function() {
        $scope.contact.$update(function(updatedRecord) {
            $scope.contact = updatedRecord;
        });
    };

    $scope.deleteContact = function() {
        $scope.contact.$delete();
        $location.url('/contacts');
    };
});

app.controller('NewContactController', function($scope, Contacts) {
    $scope.contact = new Contacts();

    $scope.addContact = function () {
        $scope.contact.save();
        $location.url('/contacts');
    };
});
