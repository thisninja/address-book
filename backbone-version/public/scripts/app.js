jBone.ajax = reqwest.compat;

var Contact = Backbone.Model.extend({});

var Contacts = Backbone.Collection.extend({
    model: Contact,
    url: '/api/contacts'
});

// ActionsView
// ContactsListView
// ContactsItemView

var ContactsListView = Backbone.View.extend({
    tagName: 'ul',

    events: {
        'click fn-show-contact-details': 'showContactDetails'
    },

    showContactDetails: function(e) {
        e.preventDefault();

        Backbone.history.navigate('contacts/' + this.model.get('id'), {trigger: true});
    },

    render: function() {
        this.collection.forEach(function(model) {
            this.$el.append(new ContactsItemVIew({model: model}).render().el);
        }, this);

        return this;
    }
});

var ActionsView = Backbone.View.extend({
    className: 'actions',

    template: '<a href="/contacts/new"> New Contact </a>',

    render: function() {
        this.el.innerHTML = this.template;

        return this;
    }

});

var ContactsItemVIew = Backbone.View.extend({
    tagName: 'li',
    template: _.template( $('#ContactsItemViewTemplate').html()),
    render: function() {
        this.el.innerHTML = this.template(this.model.toJSON());

        return this;
    }
});

var ContactView = Backbone.View.extend({
    template: _.template( $('#ContactViewTemplate').html()),

    initialize: function() {
        this.listenTo(this.model, 'change:firstName change:lastName', this.updateContactHeader);
    },

    events: {
        'click .fn-update-contact-details': 'updateContactDetails',
        'click .fn-delete-contact-details': 'deleteContactDetails'
    },

    updateContactHeader: function() {
        this.$('.contact-header').html(this.model.get('firstName') + ' ' + this.model.get('lastName'));
    },

    updateContactDetails: function() {
        this.model.save({
            firstName: this.$('.contact-firstname').val(),
            lastName: this.$('.contact-lastname').val(),
            email: this.$('.contact-email').val()
        });
    },

    deleteContactDetails: function() {
        this.model.destroy();
        Backbone.history.navigate('', {trigger: true});
    },

    render: function() {
        this.el.innerHTML = this.template(this.model.toJSON());

        return this;
    }
});

var NewContactView = Backbone.View.extend({
    template: $('#NewContactViewTemplate').html(),

    events: {
        'click .fn-create-contact': 'createContact'
    },

    createContact: function() {
        this.collection.create({
            firstName: this.$('.contact-firstname').val(),
            lastName: this.$('.contact-lastname').val(),
            email: this.$('.contact-email').val()
        }, {
            success: function() {
                Backbone.history.navigate('', {trigger: true});
            }
        });
    },

    render: function() {
        this.el.innerHTML = this.template;

        return this;
    }
});

var Router = Backbone.Router.extend({
    initialize: function(options) {
        this.contacts = options.contacts;
        this.el = $('#app');
    },

    routes: {
        '': 'main',
        'contacts': 'distplayContacts',
        'contacts/new': 'showCreateContactForm',
        'contacts/:id': 'distplaySingleContact'
    },

    main: function() {
        Backbone.history.navigate('contacts', {trigger: true});
    },

    distplayContacts: function() {
        this.el.empty()
        .append(new ActionsView().render().el)
        .append(new ContactsListView({ collection: this.contacts}).render().el);
    },

    showCreateContactForm: function() {
        this.el.empty()
            .append(new NewContactView({ collection: this.contacts}).render().el);
    },

    distplaySingleContact: function(id) {
        var model = this.contacts.get(parseInt(id, 10));

        this.el.empty().append( new ContactView({ model: model}).render().el);
    }

});

var contacts = new Contacts();

contacts.fetch().then(function(data) {
        var router = new Router({
            contacts: contacts
        });

        Backbone.history.start({pushState: true});
    });
