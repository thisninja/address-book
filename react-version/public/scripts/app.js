var ContactsList = React.createClass({
    getInitialState: function() {
        return {
            contacts : contactStore
        };
    },

    render: function() {
        var contacts = this.state.contacts.map(function (contact) {
            return (<ContactListItem key={contact.get('id')} contact={contact} />);
        });

        return (
            <div className="contact-action">
                <ReactRouter.Link to="/contacts/new"> New Contact </ReactRouter.Link>
                <ul> {contacts} </ul>
            </div>);
    }
});

var ContactListItem = React.createClass({
    render: function() {
        var contact = this.props.contact.toJSON();

        return (
            <li>
                <ReactRouter.Link to={"/contacts/" + contact.id}>

                {contact.firstName} {contact.lastName}

                <span className="contact-email">
                    {contact.email}
                </span>

                </ReactRouter.Link>
            </li>
        );
    }
});

var SingleContact = React.createClass({
    contextTypes: {
        router: React.PropTypes.func
    },

    getInitialState: function() {
        var contactId = parseInt    (this.context.router.getCurrentParams().id, 10);

        return {
            contact: contactStore.get(contactId)
        };
    },

    render: function() {
        var contact = this.state.contact.toJSON();

        return (
            <div>
                <h2> {contact.firstName} {contact.lastName} </h2>

                <p> <label> First Name: </label> <input type="text" ref="firstName" defaultValue={contact.firstName} /> </p>
                <p> <label> Last Name: </label> <input type="text" ref="lastName" defaultValue={contact.lastName} /> </p>
                <p> <label> Email: </label> <input type="email" ref="email" defaultValue={contact.email} /> </p>

                <p>
                    <button onClick={this.updateContact}> Update Contact </button>
                    <button onClick={this.deleteContact}> Delete Contact </button>
                </p>
            </div>
        );
    },

    updateContact: function () {
        this.state.contact.save({
            firstName: React.findDOMNode(this.refs.firstName).value,
            lastName: React.findDOMNode(this.refs.lastName).value,
            email: React.findDOMNode(this.refs.email).value
        }).then(function () {
            // update state for correct H2 reflecting after we'll successfully fetch data
            this.setState({
                contact: this.state.contact
            });
        }.bind(this));
    },

    deleteContact: function () {
        this.state.contact.destroy().then(function () {
            this.context.router.transitionTo('/contacts');
        }.bind(this));
    }
});

var NewContact = React.createClass({
        contextTypes: {
            router: React.PropTypes.func
        },

        render: function() {
            return (
                <div>
                    <h2> New Contact </h2>

                    <p> <label> First Name: </label> <input type="text" ref="firstName"/> </p>
                    <p> <label> Last Name: </label> <input type="text" ref="lastName" /> </p>
                    <p> <label> Email: </label> <input type="email" ref="email"  /> </p>

                    <p>
                        <button onClick={this.addContact}> Add Contact </button>
                    </p>
                </div>
            );
        },

        addContact: function () {
            contactStore.create({
                firstName: React.findDOMNode(this.refs.firstName).value,
                lastName: React.findDOMNode(this.refs.lastName).value,
                email: React.findDOMNode(this.refs.email).value
            }, {
                success: function () {
                    this.context.router.transitionTo('/contacts');
                }.bind(this)
            });
        }
});

var App = React.createClass({
    render: function() {
        return (
            <div>
                <header>
                    <h1> <ReactRouter.Link to="/contacts"> React Address Book </ReactRouter.Link> </h1>
                </header>
                <ReactRouter.RouteHandler />
            </div>
        );
    }
});

var routes = (<ReactRouter.Route handler={App}>
    <ReactRouter.Route name="contacts" handler={ContactsList} />
    <ReactRouter.Route name="new-contact" path="contacts/new" handler={NewContact} />
    <ReactRouter.Route name="contact" path="contacts/:id" handler={SingleContact} />
    <ReactRouter.Redirect from="/" to="/contacts" />
</ReactRouter.Route>);

var ContactStore = Backbone.Collection.extend({
    url: '/api/contacts'
});

var contactStore = new ContactStore();

contactStore.fetch().then(function () {
    ReactRouter.run(routes, ReactRouter.HistoryLocation, function (Handler) {
        React.render(<Handler />, document.getElementById('react-address-book'));
    });
});
