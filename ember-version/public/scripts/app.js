var app = Ember.Application.create();

app.Store = DS.Store.extend({
    adapter: DS.RESTAdapter
});

app.ApplicationAdapter = DS.RESTAdapter.extend({
    namespace: 'api'
});

app.Contact = DS.Model.extend({
    firstName: DS.attr('string'),
    lastName: DS.attr('string'),
    email: DS.attr('string'),
});

app.ContactSerializer = DS.RESTSerializer.extend({
    extractArray: function (store, primaryType, payload) {
        // overide payload to expected format
        payload = { contacts: payload };

        return this._super(store, primaryType, payload);
    },

    extractSingle: function (store, primaryType, payload, recordId) {
        // overide payload to expected format
        payload = { contact: payload };

        return this._super(store, primaryType, payload, recordId);
    },

    serializeIntoHash: function (hash, type, snapshot, options) {
        // serialize our model to JSON
        var json = this.serialize(snapshot, { includeId: true });

        // copy all properties of our json onto hash Object
        Object.keys(json).forEach(function (key) {
            hash[key] = json[key];
        });
    }
});

/*
 GET /api/contacts by default returns [...] but our model expects suggested format: { contacts: [...] }

 The same with particular contact...

 GET/POST /api/contact by default returns {}...} but our model expects suggested format: { contact: {}...} }

 2 ways to solve this: fix our backend or create serializer to convert data correctly
*/

app.Router.reopen({
    location: 'history'
});

app.Router.map(function () {
    this.resource('contacts');
    this.resource('contact', { path: 'contacts/:contact_id' });
    this.route('new', { path: 'contacts/new' });
});

// IndexRoute special route name for handling default route
app.IndexRoute = Ember.Route.extend({
    redirect: function () {
        this.transitionTo('contacts');
    }
});

app.ContactsRoute = Ember.Route.extend({
    model: function () {
        // find contact model in global store
        return this.store.find('contact');
    }
});

app.ContactController = Ember.Controller.extend({
    actions: {
        updateContact: function () {
            var model = this.get('model');

            model.save();
        },

        deleteContact: function () {
            var model = this.get('model');

            model.deleteRecord();
            model.save();

            this.transitionToRoute('contacts');
        }
    }
});

app.NewRoute = Ember.Route.extend({
    model: function () {
        // add new contact model tp global store
        return this.store.createRecord('contact');
    }
});

app.NewController = Ember.Controller.extend({
    actions: {
        addContact: function () {
            this.get('model').save();
            this.transitionToRoute('contacts');
        }
    }
});
